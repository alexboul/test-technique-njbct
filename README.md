# Informations

*Auteur : Alexandre Boulenouar*

Ce fichier contient les instructions pour le lancement de l'application ainsi que quelques informations sur le développement et les choix qui ont été fait.

## 1. Connexion à une BDD MongoDB

Le backend de l'application utilise une base de données MongoDB pour stocker les données.
Il faut que l'application puisse d'y connecter pour pouvoir fonctionner. Pour ce faire, remplacer la valeur de la constante `MONGODB_URI` par une URI MongoDB valide.

## 2. Installation des dépendances

Se placer dans le répertoire `back` et entrer la commande `npm install`

Se placer dans le répertoire `front` et entrer la commande `npm install`

## 3. Démarrage de l'application

Commencer par démarrer le back en se plaçant dans le répertoire `back` et en entrant la commande `npm run start`

Ensuite, se placer dans le répertoire `front` et entrer la commande `npm run start`

## 4. Accéder à l'application

L'application est disponible à l'URL `http://localhost:4200/`

Les deux composants attendus sont accessibles via le panneau latéral et répondent aux attentes fonctionnelles.

La documentation Swagger est disponible à l'URL `http://localhost:3000/api/docs/`

## Détails sur les choix de développement

**Base de données**

J'ai remarqué différentes caractéristiques de l'application et j'avais plusieurs critères qui m'ont aiguillé dans mon choix. Il n'y à qu'une seule entité, donc pas de relation, il fallait que ça soit simple d'utilisation et rapide à mettre en place, préférablement hébergé dans le cloud, et gratuit. MongoDB m'a donc semblé être un choix adéquat. Le seul défaut qui n'a pas été traité est la question de l'auto-incrémentation des ID des entités créées. Il est donc nécessaire d'intégrer un ID à la création d'un produit, mais il faudrait idéalement que cette opération soit automatisée.

**Backend**

Pour le backend, j'ai fais le choix de Node avec Express car c'est la technologie qui me plait le plus parmi celles proposées, et celle avec laquelle j'ai le plus envie de travailler. C'est également léger et rapide à mettre en place, ce qui m'a semblé opportun au vu de la nature de l'application. 

**Frontend**

J'ai utilisé les composants mis à disposition (Table et List qui reprennent les composant PrimeNG Table et DataView). J'ai rencontré quelques soucis durant certaines intéractions avec ces composants, que je devrais corriger en y passant un peu plus de temps dans le cadre d'une application destinée à la production.
- Dans la liste de produits, le bouton Sort by ne fonctionne pas, la liste reste sur son ordre par défaut. 
- Dans la partie Admin, à la création d'un nouveau produit, le formulaire est rempli avec des données par défaut, mais il faut cliquer sur chaque champ et en sortir avant de valider le formulaire pour qu'ils soient pris en compte pour la création du produit, sans quoi les champs seront considérés comme vide.