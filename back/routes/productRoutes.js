const express = require("express");
const { getProducts, getProduct, createProduct, deleteProduct, updateProduct } = require("../controllers/productController");
const router = express.Router();

/**
 * @swagger
 * /api/products:
 *   get:
 *     summary: Get the list of all products
 *     description: Get the list of all products
 *     responses:
 *       200:
 *         description: List of all products
 *       500:
 *         description: Server Error
 */
router.get("/", getProducts);

/**
 * @swagger
 * /api/products/{id}:
 *   get:
 *     summary: Get a single product
 *     description: Get a single product by its ID
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the product
 *         type: number
 *     responses:
 *       200:
 *         description: A single product
 *       404:
 *         description: Product not found
 *       500:
 *         description: Server Error
 */
router.get("/:id", getProduct);

/**
 * @swagger
 * /api/products:
 *   post:
 *     summary: Create a new product
 *     description: Create a new product
 *     parameters:
 *      - in: formData
 *        name: id
 *        type: number
 *        required: true
 *      - in: formData
 *        name: code
 *        type: string
 *        required: true
 *      - in: formData
 *        name: name
 *        type: string
 *        required: true
 *      - in: formData
 *        name: description
 *        type: string
 *        required: true
 *      - in: formData
 *        name: price
 *        type: number
 *        required: true
 *      - in: formData
 *        name: quantity
 *        type: number
 *        required: true
 *      - in: formData
 *        name: inventoryStatus
 *        type: string
 *        required: true
 *      - in: formData
 *        name: category
 *        type: string
 *        required: true
 *      - in: formData
 *        name: image
 *        type: string
 *      - in: formData
 *        name: rating
 *        type: number
 *     responses:
 *       201:
 *         description: The created product
 *       400:
 *         description: Error during product creation
 *       500:
 *         description: Server Error
 */
router.post("/", createProduct);

/**
 * @swagger
 * /api/products/{id}:
 *   delete:
 *     summary: Delete a product
 *     description: Delete a product by its ID
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the product
 *         type: number
 *     responses:
 *       200:
 *         description: The deleted product
 *       400:
 *         description: Error during product deletion
 *       500:
 *         description: Server Error
 */
router.delete("/:id", deleteProduct);

/**
 * @swagger
 * /api/products/{id}:
 *   patch:
 *     summary: Update a product
 *     description: Update a product by its ID
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the product
 *        type: number
 *      - in: formData
 *        name: id
 *        type: number
 *        required: true
 *      - in: formData
 *        name: code
 *        type: string
 *        required: true
 *      - in: formData
 *        name: name
 *        type: string
 *        required: true
 *      - in: formData
 *        name: description
 *        type: string
 *        required: true
 *      - in: formData
 *        name: price
 *        type: number
 *        required: true
 *      - in: formData
 *        name: quantity
 *        type: number
 *        required: true
 *      - in: formData
 *        name: inventoryStatus
 *        type: string
 *        required: true
 *      - in: formData
 *        name: category
 *        type: string
 *        required: true
 *      - in: formData
 *        name: image
 *        type: string
 *      - in: formData
 *        name: rating
 *        type: number
 *     responses:
 *       200:
 *         description: The updated product
 *       400:
 *         description: Error during product update
 *       500:
 *         description: Server Error
 */
router.patch("/:id", updateProduct);

module.exports = router;
