const mongoose = require("mongoose");
mongoose.set("strictQuery", true);

const MONGODB_URI = '<VOTRE_URI_MONGODB>'

const connectDB = async () => {
    try {
        const connect = await mongoose.connect(MONGODB_URI);
        console.log(`Connected to MongoDB : ${connect.connection.host}`);
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}
module.exports = connectDB;