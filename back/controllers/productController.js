const asyncHandler = require("express-async-handler");
const Product = require("../models/productModel");

const getProducts = asyncHandler(async (req, res) => {
    await Product.find().then((products) => {
        res.status(200).json(products);
    });
});

const getProduct = asyncHandler(async (req, res) => {
    const id = req.params.id;
    const product = await Product.findOne({id: id})
    if(!product) {
        res.status(404).json({message: "Product not found"});
    } else {
        res.status(200).json(product);
    }
});

const createProduct = asyncHandler(async (req, res) => {
    const productExists = await Product.findOne({id: req.body.id});
    if(productExists) {
        res.status(400).json({message: 'Product with this id already exists'});
    } else {
        const product = await Product.create({
                id: req.body.id,
                code: req.body.code,
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
                quantity: req.body.quantity,
                inventoryStatus: req.body.inventoryStatus,
                category: req.body.category,
                image: req.body.image ? req.body.image : undefined,
                rating: req.body.rating ? req.body.rating : undefined
            });
        if(!product) {
            res.status(400).json({message: 'Error during product creation'}); 
        } else {
            res.status(201).json(product);
        }
    }
});

const deleteProduct = asyncHandler(async (req, res) => {
    const id = req.params.id;
    const product = await Product.findOne({id: id});

    if(!product) {
        res.status(400).json({message: 'Error during product deletion'}); 
    } else {
        await Product.deleteOne({id: id})
        res.status(200).json(product)
    }
});

const updateProduct = asyncHandler(async (req, res) => {
    const id = req.params.id
    const product = await Product.findOne({id: id});

    if(!product) {
        res.status(400).json({message: 'Error during product update'})
    } else {
       const updatedProduct = await Product.findOneAndUpdate({id: id}, req.body, {returnOriginal: false})
       res.status(200).json(updatedProduct);
    }
});


module.exports = {
    createProduct,
    getProducts,
    getProduct,
    deleteProduct,
    updateProduct
}