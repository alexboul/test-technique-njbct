const express = require('express');
const connectDB = require('./config.db');
const cors = require("cors");
const PORT = 3000;
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Alten Shop API",
            description: "API Rest pour l'application Alten Shop, développée avec Express",
            contact: {
                name: "Alexandre Boulenouar"
            },
            servers: ["http://localhost:3000"]
        }
    },
    apis: ["./routes/*.js"]
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);

connectDB();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.use("/api/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.use("/api/products", require("./routes/productRoutes"));

app.listen(PORT, () => {
    console.log('Listening port : ', PORT);
});