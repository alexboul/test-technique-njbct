import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Product } from 'app/models/product.model';
import { ProductService } from 'app/services/product.service';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @ViewChild("gridTemplate") gridTemplate: TemplateRef<any>;
  @ViewChild("listTemplate") listTemplate: TemplateRef<any>;

  products: Product[];
  totalRecords: number;
  sortOptions: SelectItem[] = [
    { label: 'Name', value: 'name' },
    { label: 'Price', value: 'price' },
    { label: 'Category', value: 'category' }
  ];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.fetchProducts().subscribe((products) => {
      this.products = products;
      this.totalRecords = products.length;
    });
  }
}
