import { Component, OnInit } from '@angular/core';
import { Product } from 'app/models/product.model';
import { ProductService } from 'app/services/product.service';
import { ControlType } from 'app/shared/utils/crud-item-options/control-type.model';
import { CrudItemOptions } from 'app/shared/utils/crud-item-options/crud-item-options.model';
import { SnackbarService } from 'app/shared/utils/snackbar/snackbar.service';

@Component({
  selector: 'app-products-admin',
  templateUrl: './products-admin.component.html',
  styleUrls: ['./products-admin.component.scss']
})
export class ProductsAdminComponent implements OnInit {

  products: Product[];
  config: CrudItemOptions[];
  entity = Product;

  constructor(private productService: ProductService, private snackbarService: SnackbarService) {
    this.buildTableConfig();
  }

  ngOnInit(): void {
    this.loadData();
  }


  onSave(event: {entry: Partial<Product>, creation: boolean}): void {
    if(event.creation) {
      this.productService.createProduct(event.entry).subscribe({
        next: () => {
          this.loadData();
          this.snackbarService.displaySuccess();
        },
        error: () => {
          this.snackbarService.displayError();
        }
      }
    );
  
    } else {
      this.productService.updateProduct(event.entry.id, event.entry).subscribe(
        {
          next: () => {
            this.loadData();
            this.snackbarService.displaySuccess();
          },
          error: () => {
            this.snackbarService.displayError();
          }
        }
      );
    }
  }

  onDelete(event: number[]): void {
    event.forEach((id: number) => {
      this.productService.deleteProduct(id).subscribe(
        {
          next: () => {
            this.loadData();
            this.snackbarService.displaySuccess();
          },
          error: () => {
            this.snackbarService.displayError();
          }
        }
      );
    });
  }

  private loadData(): void {
    this.productService.fetchProducts().subscribe((products: Product[]) => {
      this.products = products;
    });
  }

  private buildTableConfig(): void {
    this.config = [
      {
        key: 'id',
        label: 'ID',
        controlType: ControlType.INPUT,
        type: 'number',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      {
        key: 'code',
        label: 'Code',
        controlType: ControlType.INPUT,
        type: 'text',
        columnOptions: {
          sortable: true,
          filterable: true,
          default: true,
          hidden: false
        }
      },
      {
        key: 'name',
        label: 'Name',
        controlType: ControlType.INPUT,
        type: 'text',
        columnOptions: {
          sortable: true,
          filterable: true,
          default: true,
          hidden: false
        }
      },
      {
        key: 'description',
        label: 'Description',
        controlType: ControlType.INPUT,
        type: 'text',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      {
        key: 'inventoryStatus',
        label: 'Inventory status',
        controlType: ControlType.SELECT,
        options: [
          {label: 'INSTOCK', value: 'INSTOCK'},
          {label: 'OUTOFSTOCK', value: 'OUTOFSTOCK'}, 
          {label: 'LOWSTOCK', value: 'LOWSTOCK'}
        ],
        type: 'text',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      {
        key: 'image',
        label: 'Image URL',
        controlType: ControlType.INPUT,
        type: 'text',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      {
        key: 'price',
        label: 'Price',
        controlType: ControlType.INPUT,
        type: 'number',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      {
        key: 'category',
        label: 'Category',
        controlType: ControlType.INPUT,
        type: 'text',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      {
        key: 'quantity',
        label: 'Quantity',
        controlType: ControlType.INPUT,
        type: 'number',
        columnOptions: {
          default: false,
          hidden: false
        }
      },
      
    ]
  }
}
