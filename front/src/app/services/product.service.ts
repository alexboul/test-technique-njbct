import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from 'app/models/product.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private static url = 'http://localhost:3000/api/products/';

  constructor(private http: HttpClient) { }

  fetchProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(ProductService.url);
  }

  getProductById(id: number): Observable<Product> {
    return this.http.get<Product>(`${ProductService.url}/${id}`);
  }

  createProduct(product: Partial<Product>): Observable<Product> {
    return this.http.post<Product>(ProductService.url, product);
  }

  deleteProduct(id: number): Observable<object> {
    return this.http.delete(`${ProductService.url}/${id}`);
  }

  updateProduct(id: number, product: Partial<Product>): Observable<Product> {
    return this.http.patch<Product>(`${ProductService.url}/${product.id}`, product)
  }
}
